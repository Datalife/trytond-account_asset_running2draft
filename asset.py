# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, Workflow
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Asset(metaclass=PoolMeta):
    __name__ = 'account.asset'

    @classmethod
    def __setup__(cls):
        super(Asset, cls).__setup__()
        cls._transitions |= set((
            ('running', 'draft'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'running',
                'icon': 'tryton-back'
            },
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, assets):
        for asset in assets:
            if asset.move or any(l.move for l in asset.lines):
                raise UserError(gettext(
                    'account_asset_running2draft.msg_cannot_draft_asset',
                    asset=asset.rec_name))


class UpdateAssetShowDepreciation(metaclass=PoolMeta):
    __name__ = 'account.asset.update.show_depreciation'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.depreciation_account.readonly = False
