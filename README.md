datalife_account_asset_running2draft
====================================

The account_asset_running2draft module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_asset_running2draft/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_asset_running2draft)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
